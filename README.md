# VARYS Monitoring Actuation Bridge

## Synopsis

This main goal of the Monitoring Actuation Bridge (MAB) component is to hide the complexity
of shipping and managing monitoring probes for monitoring targets deployed on different platforms.

The MAB exposes a gRPC interface which abstracts from the underlying platform technology.
The specific implementation is provided by plug-ins discovered during the bootstrap of the application.

## Plug-ins
The MAB exploits [Pluginlib](https://pluginlib.readthedocs.io/en/stable/index.html) to define and access the plug-ins.

At the moment, it loads all the plug-ins from the `app.plugins` package. The base plug-in interface is defined by the `app.plugins.base.IMonitorablePlatformPlugin` class.

### Kubernetes
The Kubernetes plug-in is able to manage multiple clusters provided in a _kube-config_ file.
It expects the context namespace to be set, otherwise it will use _default_.

#### Current supported monitorable targets
* Deployment
* StatefulSet

#### Current supported monitoring patterns
* **Reserved sidecar single probe**: dedicated (1 for each monitoring request) sidecar container added to the Pod. It can mount hostPath and configMap volumes.

#### Settings
##### Environment variables

| Name | Required | Default |
| - | - | - |
| KUBERNETES_CONFIG_FILE | NO | ~/.kube/config |

#### Probe templates

TBD

### OpenStack
The OpenStack plug-in is able to manage multiple clouds provided in a _clouds.yml_ file. However, currently it supports only cloud.
It exploits [openstacksdk](https://docs.openstack.org/openstacksdk/stein/) and [ansible_runner](https://ansible-runner.readthedocs.io/en/stable/) libraries to perform API operations and to configure/start/stop the probes respectively.

#### Current supported monitarable targets
* Accessible VM
* Inaccessible VM

A metadata has to be specified for the servers in order to distinguish between accessible and inaccessible VMs. The [Target metadata](#Target-metadata) section will provide further details about the required metadata.

#### Current supported monitoring patterns
* **Reserved sidecar multi-probe**: dedicated (1 for each monitoring request) sidecar VM that holds one or more probes.
* **Internal probes**: the probes are directly executed within the target. They are shared across multiple monitoring requests.

#### Settings
##### Environment variables

| Name | Required | Default |
| - | - | - |
| OS_CLOUD | YES (for the moment) | - |
| OS_CLIENT_CONFIG_FILE | NO | ~/.config/openstack/clouds.yaml |
| OS_MONITORING_NETWORK_NAME | NO | monitoring-network |
| OS_MONITORING_PRIVATE_KEY | NO | ~/.ssh/monitoring-key |
| OS_FLOATING_NETWORK_NAME | NO | floating-ip|
| OS_RESERVED_SIDECAR_IMAGE_NAME | YES | - |
| OS_RESERVED_SIDECAR_FLAVOR_NAME | YES | - |
| OS_RESERVED_SIDECAR_USERNAME | NO | root |
| OS_ANSIBLE_VAULT_PASSWORD | NO | secret |

#### Probe templates

TBD

## Target metadata
| Name | Required | Accepted values | Default |
| - | - | - | - |
| varys-monitorable-target | YES | true, false | - |
| varys-environment-type | YES | CONTAINER, ACCESSIBLE_VM, INACCESSIBLE_VM | - |

## Development
In order to create a reproducible environment a [Dockerfile](src/Dockerfile) is available along with `docker-compose*.yml` files.

The `docker-compose.override.yml` file holds the development configurations such as ports binding, build from sources or local volumes.

The `dev-configs` directory can be used to store files and assets needed for development purposes. In the `docker-compose.override.yml` is available an example usage of `dev-configs` sub-directories binded as volumes to provide Kubernetes plug-in configuration files. The content of the `dev-configs` directory is ignored by git.

> N.B.: all the commands are intended to be executed from the project root directory (`varys-monitoring-actuation-bridge/`)

Requirements to run with Docker Compose:

* Docker Engine 18.02.0+
* Docker Compose 1.20.0+

Run the following command to build the services:
```
$ docker-compose build
```

Then, run the following command to run the services:
```
$ docker-compose up
```

## License
This project is licensed under the AGPLv3. See the [LICENSE.md](LICENSE.md) file for details.
