# -*- coding: utf-8 -*-
import pytest

@pytest.fixture
def plugin():
    from app.plugins.kubernetes.plugin import Kubernetes
    return Kubernetes()

def test_list_monitorable_targets(plugin):
    targets = plugin.list_monitorable_targets()
    assert isinstance(targets, list)

# def test_create_reserved_sidecar(plugin):
#     monitoring_object_id = plugin.create_reserved_sidecar(
#       '1',
#       'nginx-deployment',
#       { # Probes map with probes to be added
#         'cpu_consumption': {
#           'probe_id': '1',
#           'probe_type': 'metricbeat_cpu_consumption',
#           'cloud_properties': ['cpu_consumption']
#         }
#       }
#     )
#     assert monitoring_object_id == 'nginx-deployment'

# def test_update_reserved_sidecar(plugin):
#     monitoring_object_id = plugin.update_reserved_sidecar(
#       '1',
#       'nginx-deployment',
#       { # Probes map with probes to be added
#         'memory_consumption': {
#           'probe_id': '1',
#           'probe_type': 'metricbeat_memory_consumption',
#           'cloud_properties': ['memory_consumption']
#         }
#       },
#       { # Probes map with probes to be removed
#         'cpu_consumption': {
#           'probe_id': '1',
#           'probe_type': 'metricbeat_cpu_consumption',
#           'cloud_properties': ['cpu_consumption']
#         }
#       }
#     )
#     assert monitoring_object_id == 'nginx-deployment'

# def test_delete_reserved_sidecar(plugin):
#   plugin.delete_reserved_sidecar(
#     '1',
#     'nginx-deployment',
#     { # Probes map with probes to be removed
#       'memory_consumption': {
#         'probe_id': '1',
#         'probe_type': 'metricbeat_memory_consumption',
#         'cloud_properties': ['memory_consumption']
#       }
#     }
#   )
#
#   assert 1
