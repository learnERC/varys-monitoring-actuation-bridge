# -*- coding: utf-8 -*-

import logging
import sys
import uuid
import shutil
from distutils.util import strtobool
import os
from os import listdir, makedirs, chmod
from os.path import dirname, abspath, join, isfile, expanduser, isdir
from tempfile import NamedTemporaryFile, mkdtemp
from time import sleep
import socket
import string_utils
from simple_settings import settings
from jinja2 import Environment, FileSystemLoader
from ruamel.yaml import YAML
import openstack
from openstack.config import loader
import app.plugins.base as base_plugins
from app.api import monitoring_actuation_bridge_pb2 as protobuf_messages
import ansible_runner
from paramiko import SSHClient, AutoAddPolicy
from paramiko.ssh_exception import BadHostKeyException, AuthenticationException, SSHException

class OpenStack(base_plugins.IMonitorablePlatformPlugin):

  _alias_ = 'openstack'
  _version_ = '1.0.0'

  TEMPLATES_DIR = join(dirname(abspath(__file__)), 'templates')
  ROLES_DIR = join(dirname(abspath(__file__)), 'roles')

  def __init__(self):
    super().__init__()
    openstack.enable_logging(False, stream=sys.stdout)
    config = loader.OpenStackConfig()
    self.cloud = openstack.connect(cloud=settings.OS_CLOUD)

  def list_targets(self, filters={}):
    # Filters are currently unsupported
    targets = []
    try:
      servers = self.cloud.compute.servers(**{'power_state': 1, 'status': 'ACTIVE'})
      servers = self._filter_out_not_monitorable_servers(servers)
      for server in servers:
        targets.append(protobuf_messages.Target(
          id=server.id,
          name=server.name,
          source=self.name,
          environment=self._extract_environment_metadata(server.metadata),
          metadata=server.metadata,
        ))
    except Exception as e:
      logging.error('An error occurred during listing the targets for {} {} plugin: {}'.format(self._alias_, self._version_, e))
    finally:
      return targets

  def _filter_out_not_monitorable_servers(self, servers):
    monitorable_servers = []
    for server in servers:
      if not server.metadata:
        continue
      elif not server.metadata.get('varys-monitorable-target', False):
        continue
      elif not strtobool(server.metadata.get('varys-monitorable-target')):
        continue
      else:
        monitorable_servers.append(server)
    return monitorable_servers

  def _extract_environment_metadata(self, metadata):
    if str(metadata.get('varys-environment-type')) == str(protobuf_messages.Environment.Name(protobuf_messages.ACCESSIBLE_VM)):
      return protobuf_messages.ACCESSIBLE_VM
    elif str(metadata.get('varys-environment-type')) == str(protobuf_messages.Environment.Name(protobuf_messages.INACCESSIBLE_VM)):
      return protobuf_messages.INACCESSIBLE_VM
    else:
      return protobuf_messages.ACCESSIBLE_VM

  def get_monitoring_system_status(self, monitoring_request_id, target, pattern, probes, monitoring_units, data_engine):
    # Only global status is currently supported
    runner_input_dir_prefix = 'ansible_runner_{}_'.format(monitoring_request_id)
    tmp_dirs = listdir('/tmp')
    for dir in tmp_dirs:
      if isdir(join('/tmp', dir)) and dir.startswith(runner_input_dir_prefix):
        return protobuf_messages.DEPLOYING

    if pattern == protobuf_messages.INTERNAL_PROBES:
      return protobuf_messages.DEPLOYED

    monitoring_unit = self.cloud.compute.get_server(monitoring_units[0].id)
    if monitoring_unit.status == 'ACTIVE' and monitoring_unit.power_state == 1:
      return protobuf_messages.DEPLOYED
    else:
      return protobuf_messages.ERROR

  def apply_reserved_sidecar_single_probe_pattern(self, monitoring_request_id, target, probes_to_add, probes_to_remove, monitoring_units, data_engine):
    raise NotImplementedError(
      'The reserved_sidecar_single_probe pattern is not supported by the {} {} plugin'.format(self._alias_, self._version_)
    )

  def apply_internal_probes_pattern(self, monitoring_request_id, target, probes_to_add, probes_to_remove, monitoring_units, data_engine):
    probe_roles_to_add = [probe.name for probe in probes_to_add]
    probe_roles_to_remove = [probe.name for probe in probes_to_remove]
    vars = {
      'target_id': target.id
    }
    target = self.cloud.compute.find_server(name_or_id=target.id)
    with open(expanduser(settings.OS_MONITORING_PRIVATE_KEY), 'r') as f:
      private_key = f.read()

    runner_input_dir = self._build_ansible_runner_input_dir(
      monitoring_request_id=monitoring_request_id,
      hostnames=[target.addresses['default'][1]['addr']],
      playbook_name='playbook-internal.yml.j2',
      probe_roles_to_add=probe_roles_to_add,
      probe_roles_to_remove=probe_roles_to_remove,
      private_key=private_key,
      vars=vars,
      passwords={}
    )

    # FIXME: Switch to run_async? What can happen if the MaaS ask for status to another MAB instance?
    runner_results = ansible_runner.interface.run(private_data_dir=runner_input_dir, roles_path=[self.ROLES_DIR], playbook='playbook-internal.yml')
    logging.debug('Ansible runner status: {}'.format(runner_results.status))
    logging.debug('Ansible runner stats: {}'.format(runner_results.stats))

    shutil.rmtree(runner_input_dir, ignore_errors=True)

    return None

  def apply_reserved_sidecar_multi_probes_pattern(self, monitoring_request_id, target, probes_to_add, probes_to_remove, monitoring_units, data_engine):
    if not monitoring_units:
      # The reserved sidecar has to be created
      keypair, sidecar, floating_ip_address = self._create_reserved_sidecar(monitoring_request_id)
      private_key = keypair.private_key
      # Create the monitoring unit to be returned
      monitoring_units = []
      monitoring_units.append(
        protobuf_messages.MonitoringUnit(
          id=sidecar.id,
          name=sidecar.name,
          probe_ids=[probe.id for probe in probes_to_add]
        )
      )
    elif not probes_to_add and len(probes_to_remove) == len(monitoring_units[0].probe_ids):
      # The reserved sidecar has to be removed
      sidecar = self.cloud.compute.find_server(name_or_id=monitoring_units[0].id)
      self.cloud.compute.delete_server(sidecar)
      return None
    else:
      # The reserved sidecar has to be updated
      sidecar = self.cloud.compute.find_server(name_or_id=monitoring_units[0].id)
      # FIXME: Implement a more safe strategy to get the IP address
      floating_ip_address = sidecar.addresses['default'][1]['addr']
      private_key = monitoring_units[0].credentials['private_key']
      # Calculate the current probe ids of the monitoring unit to be returned
      probe_ids_to_add = [probe.id for probe in probes_to_add]
      probe_ids_to_remove = [probe.id for probe in probes_to_remove]
      current_probe_ids = probe_ids_to_add + [id for id in monitoring_units[0].probe_ids if id not in probe_ids_to_remove]
      del monitoring_units[0].probe_ids[:]
      monitoring_units[0].probe_ids.extend(current_probe_ids)

    probe_roles_to_add = [probe.name for probe in probes_to_add]
    probe_roles_to_remove = [probe.name for probe in probes_to_remove]
    vars = {
      'monitoring_request_id': monitoring_request_id
    }

    runner_input_dir = self._build_ansible_runner_input_dir(
      monitoring_request_id=monitoring_request_id,
      hostnames=[floating_ip_address],
      playbook_name='playbook-sidecar.yml.j2',
      probe_roles_to_add=probe_roles_to_add,
      probe_roles_to_remove=probe_roles_to_remove,
      private_key=private_key,
      vars=vars,
      passwords={}
    )

    # FIXME: Switch to run_async? What can happen if the MaaS ask for status to another MAB instance?
    runner_results = ansible_runner.interface.run(private_data_dir=runner_input_dir, roles_path=[self.ROLES_DIR], playbook='playbook-sidecar.yml')

    logging.debug('Ansible runner status: {}'.format(runner_results.status))
    logging.debug('Ansible runner stats: {}'.format(runner_results.stats))

    shutil.rmtree(runner_input_dir, ignore_errors=True)

    return monitoring_units

  def reconfigure_prometheus(self, monitoring_request_id, hosts, credentials, config_location):
    raise NotImplementedError(
      'Reconfigure Prometheus method is not implemented by the {} {} plugin'.format(self._alias_, self._version_)
    )

  def _build_ansible_runner_input_dir(self, monitoring_request_id, hostnames, playbook_name, probe_roles_to_add, probe_roles_to_remove, private_key, vars={}, passwords={}):
    input_dir_prefix = 'ansible_runner_{}_'.format(monitoring_request_id)
    runner_input_dir = mkdtemp(prefix=input_dir_prefix)

    env_dir = '{}/env'.format(runner_input_dir)
    inventory_dir = '{}/inventory'.format(runner_input_dir)
    project_dir = '{}/project'.format(runner_input_dir)

    self._generate_playbook(project_dir, playbook_name, probe_roles_to_add, probe_roles_to_remove)
    self._generate_inventory(inventory_dir, hostnames)
    self._generate_env(env_dir, vars, passwords, private_key)

    return runner_input_dir

  def _generate_env(self, env_dir, vars, passwords, private_key):
    self._make_named_template(template_name='envvars.j2', templates_args={'envvars': {}}, output_dir=env_dir, template_dir='env')
    self._make_named_template(template_name='extravars.j2', templates_args={'extravars': vars}, output_dir=env_dir, template_dir='env')
    self._make_named_template(template_name='cmdline.j2', templates_args={}, output_dir=env_dir, template_dir='env')
    self._make_named_template(template_name='passwords.j2', templates_args={'passwords': passwords}, output_dir=env_dir, template_dir='env')
    self._make_named_template(template_name='ssh_key.j2', templates_args={'ssh_key': private_key}, output_dir=env_dir, template_dir='env')
    chmod('{}/ssh_key'.format(env_dir), 0o400)

  def _generate_inventory(self, inventory_dir, hostnames):
    self._make_named_template(
      template_name='hosts.j2',
      templates_args={'hostnames': hostnames},
      output_dir=inventory_dir,
      template_dir='inventory'
    )

  def _generate_playbook(self, project_dir, playbook_name, probe_roles_to_add, probe_roles_to_remove):
    self._make_named_template(
      template_name=playbook_name,
      templates_args={'probe_roles_to_remove': probe_roles_to_remove, 'probe_roles_to_add': probe_roles_to_add},
      output_dir=project_dir,
      template_dir='project'
    )

  def _make_named_template(self, template_name, templates_args, output_dir, template_dir=''):
    jinja_env = Environment(loader=FileSystemLoader(self.TEMPLATES_DIR), trim_blocks=True)
    if template_dir != '':
      full_template_name = '{}/{}'.format(template_dir, template_name)
    else:
      full_template_name = template_name
    template = jinja_env.get_template(full_template_name)
    output = template.render(**templates_args)

    template_filename = '{}/{}'.format(output_dir, template_name.strip('.j2'))
    makedirs(output_dir, exist_ok=True)
    with open(template_filename, 'w+') as f:
      f.write(output)

  def _create_reserved_sidecar(self, monitoring_request_id):
    sidecar_name = '{}-reserved-sidecar'.format(monitoring_request_id)
    image = self.cloud.compute.find_image(settings.OS_RESERVED_SIDECAR_IMAGE_NAME)
    flavor = self.cloud.compute.find_flavor(settings.OS_RESERVED_SIDECAR_FLAVOR_NAME)
    # HACK:
    # IMHO, the reserved sidecar should have visibility only about the target
    # Thus, dedicated network/subnet/security_group can be created and attached
    # to the target and to the reserved sidecar
    network = self.cloud.network.find_network(settings.OS_MONITORING_NETWORK_NAME)
    networks = [{'uuid': network.id}]
    keypair = self._create_keypair(monitoring_request_id)
    metadata = {
      'monitoring_request_id': monitoring_request_id
    }

    sidecar = self._create_server(sidecar_name, image.id, flavor.id, networks, keypair.name, metadata)
    # Add a new floating ip
    network = self.cloud.network.find_network(settings.OS_FLOATING_NETWORK_NAME)
    floating_ip = self.cloud.network.create_ip(floating_network_id=network.id)
    self.cloud.compute.add_floating_ip_to_server(sidecar, floating_ip.floating_ip_address)

    if not self._is_ssh_connection_ready(floating_ip.floating_ip_address, settings.OS_RESERVED_SIDECAR_USERNAME, keypair.private_key):
      raise RuntimeError('SSH connection to {} is not ready. Max retries exceeded.'.format(floating_ip.floating_ip_address))
    return keypair, sidecar, floating_ip.floating_ip_address

  def _create_server(self, server_name, image_id, flavor_id, networks, keypair_name, metadata={}):
    server = self.cloud.compute.create_server(
      name=server_name,
      image_id=image_id,
      flavor_id=flavor_id,
      networks=networks,
      key_name=keypair_name,
      metadata=metadata
    )
    server = self.cloud.compute.wait_for_server(server)

    return server

  def _create_keypair(self, keypair_name=''):
    if not keypair_name:
      keypair_name = uuid.uuid4().hex
    keypair = self.cloud.compute.create_keypair(name=keypair_name)
    return keypair

  def _is_ssh_connection_ready(self, ip, username, private_key, initial_wait=5, interval=3, retries=20):
    tmp_keyfile = NamedTemporaryFile(mode='w+', delete=False)
    tmp_keyfile.write(private_key)
    tmp_keyfile.close()

    ssh = SSHClient()
    ssh.set_missing_host_key_policy(AutoAddPolicy())

    sleep(initial_wait)

    for r in range(retries):
      try:
        ssh.connect(ip, username=username, key_filename=tmp_keyfile.name)
        os.remove(tmp_keyfile.name)
        return True
      except (BadHostKeyException, AuthenticationException, SSHException, socket.error) as e:
        sleep(interval)
      finally:
        ssh.close()

    os.remove(tmp_keyfile.name)
    return False
