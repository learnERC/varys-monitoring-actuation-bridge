# -*- coding: utf-8 -*-

import pluginlib
import os
from app.api import monitoring_actuation_bridge_pb2 as protobuf_messages
import logging
from os.path import dirname, abspath, join, isfile, isdir, expanduser
import shutil

@pluginlib.Parent('monitorable_platform')
class IMonitorablePlatformPlugin(object):

  POST_DEPLOYMENT_DIR = '/opt/monitoring-actuation-bridge/post_deployment_artifacts'

  @pluginlib.abstractmethod
  def list_targets(self, filters={}):
    pass

  def create_monitoring_system(self, monitoring_request_id, target, pattern, probes, data_engine):
    self._build_post_deployment_dirs(monitoring_request_id, data_engine)
    pattern_name = protobuf_messages.Pattern.Name(pattern).lower()
    pattern_command = getattr(self, 'apply_{}_pattern'.format(pattern_name))
    return pattern_command(monitoring_request_id, target, probes_to_add=probes, probes_to_remove=[], monitoring_units=[], data_engine=data_engine)

  def update_monitoring_system(self, monitoring_request_id, target, pattern, probes_to_add, probes_to_remove, monitoring_units, data_engine):
    self._build_post_deployment_dirs(monitoring_request_id, data_engine)
    pattern_name = protobuf_messages.Pattern.Name(pattern).lower()
    pattern_command = getattr(self, 'apply_{}_pattern'.format(pattern_name))
    return pattern_command(monitoring_request_id, target, probes_to_add, probes_to_remove, monitoring_units, data_engine)

  def delete_monitoring_system(self, monitoring_request_id, target, pattern, probes, monitoring_units, data_engine):
    self._build_post_deployment_dirs(monitoring_request_id, data_engine)
    pattern_name = protobuf_messages.Pattern.Name(pattern).lower()
    pattern_command = getattr(self, 'apply_{}_pattern'.format(pattern_name))
    response = pattern_command(monitoring_request_id, target, probes_to_add=[], probes_to_remove=probes, monitoring_units=monitoring_units, data_engine=data_engine)

    if data_engine.needs_reconfiguration:
      self.reconfigure_data_engine(monitoring_request_id, data_engine)

    return response

  @pluginlib.abstractmethod
  def get_monitoring_system_status(self, monitoring_request_id, target, pattern, probes, monitoring_units, data_engine):
    pass

  @pluginlib.abstractmethod
  def apply_reserved_sidecar_single_probe_pattern(self, monitoring_request_id, target, probes_to_add, probes_to_remove, monitoring_units, data_engine):
    pass

  @pluginlib.abstractmethod
  def apply_reserved_sidecar_multi_probes_pattern(self, monitoring_request_id, target, probes_to_add, probes_to_remove, monitoring_units, data_engine):
    pass

  @pluginlib.abstractmethod
  def apply_internal_probes_pattern(self, monitoring_request_id, target, probes_to_add, probes_to_remove, monitoring_units, data_engine):
    pass

  def reconfigure_data_engine(self, monitoring_request_id, data_engine):
    data_engine_name = data_engine.name.lower()
    reconfigure_command = getattr(self, 'reconfigure_{}'.format(data_engine_name))
    response = reconfigure_command(monitoring_request_id, data_engine.hosts, data_engine.credentials, data_engine.config_location)

    shutil.rmtree(join(self.POST_DEPLOYMENT_DIR, monitoring_request_id))

    return response

  @pluginlib.abstractmethod
  def reconfigure_prometheus(self, monitoring_request_id, hosts, credentials, config_location):
    pass

  def _build_post_deployment_dirs(self, monitoring_request_id, data_engine):
    if data_engine.needs_reconfiguration:
      probes_to_add_dir = join(self.POST_DEPLOYMENT_DIR, monitoring_request_id, 'to_add')
      probes_to_remove_dir = join(self.POST_DEPLOYMENT_DIR, monitoring_request_id, 'to_remove')
      try:
        os.makedirs(name=probes_to_add_dir, exist_ok=True)
        os.makedirs(name=probes_to_remove_dir, exist_ok=True)
      except OSError as e:
        logging.error('Failed to create post-deployment dirs for monitoring request {}'.format(monitoring_request_id))
        raise RuntimeError(e)
