# -*- coding: utf-8 -*-

import app.plugins.base as base_plugins
from app.api import monitoring_actuation_bridge_pb2 as protobuf_messages
from simple_settings import settings
import logging
import hashlib
from kubernetes import client, config
from kubernetes.client.rest import ApiException
from jinja2 import Environment, FileSystemLoader
from ruamel.yaml import YAML
from ruamel.yaml.compat import StringIO
import string_utils
from os import listdir, walk
from os.path import dirname, abspath, join, isfile, isdir, expanduser
import requests
import time
from pathlib import Path
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler

class KubeConfigFileEventHandler(FileSystemEventHandler):
  def __init__(self, plugin):
    super().__init__()
    self.plugin = plugin

  def on_modified(self, event):
    self.plugin._create_api_clients_from_config()

class MyYAML(YAML):
  def dump(self, data, stream=None, **kw):
    inefficient = False
    if stream is None:
      inefficient = True
      stream = StringIO()
    YAML.dump(self, data, stream, **kw)
    if inefficient:
      return stream.getvalue()

class Kubernetes(base_plugins.IMonitorablePlatformPlugin):

  _alias_ = 'kubernetes'
  _version_ = '1.0.0'

  TEMPLATES_DIR = join(dirname(abspath(__file__)), 'templates')

  def __init__(self):
    super().__init__()
    self.yaml = MyYAML(typ='safe', pure=True)
    self.api_clients = {}
    self.contexts = {}

    self._create_api_clients_from_config()

    event_handler = KubeConfigFileEventHandler(self)
    observer = Observer()
    observer.schedule(event_handler, path=dirname(expanduser(settings.KUBERNETES_CONFIG_FILE)), recursive=False)
    observer.start()

  def _create_api_clients_from_config(self):
    contexts, _ = config.list_kube_config_contexts(config_file=expanduser(settings.KUBERNETES_CONFIG_FILE))
    if not contexts:
      raise RuntimeError('Cannot find any context in kube-config file')

    current_context_names = []
    for context in contexts:
      _context = context['context']
      _context['namespace'] = _context['namespace'] if _context.get('namespace') else 'default'
      _name = context['name']
      current_context_names.append(_name)
      # if not self.contexts.get(_name) or self.contexts[_name] != _context:
      #   self.contexts[_name] = _context
      #   self.api_clients[_name] = config.new_client_from_config(context=_name)
      # HACK: Recreate always contexts and clients
      self.contexts[_name] = _context
      self.api_clients[_name] = config.new_client_from_config(context=_name)

    contexts_to_remove = [name for name in self.contexts.keys() if name not in current_context_names]
    for context_name in contexts_to_remove:
      del self.contexts[context_name]
      del self.api_clients[context_name]

  def list_targets(self, filters={}):
    # Filters are currently unsupported
    targets = []
    try:
      for context, api_client in self.api_clients.items():
        apps_v1 = client.AppsV1Api(api_client)
        targets.extend(self._list_deployment_targets(context, apps_v1))
        targets.extend(self._list_stateful_set_targets(context, apps_v1))
    except Exception as e:
      logging.error('An error occurred during listing the targets for {} {} plugin: {}'.format(self._alias_, self._version_, e))
    finally:
      return targets

  def _list_stateful_set_targets(self, context, api):
    targets = []
    namespace = self.contexts[context]['namespace']
    cluster = self.contexts[context]['cluster']
    stateful_sets = api.list_namespaced_stateful_set(
      namespace=namespace,
      label_selector='varys-monitorable-target=true',
      watch=False
    )
    for stateful_set in stateful_sets.items:
      targets.append(protobuf_messages.Target(
        id=stateful_set.metadata.name,
        name=stateful_set.metadata.name,
        source=self.name,
        environment=protobuf_messages.CONTAINER,
        metadata={'cluster': cluster, 'namespace': namespace, 'context': context, 'kind': 'StatefulSet'},
      ))
    return targets

  def _list_deployment_targets(self, context, api):
    targets = []
    namespace = self.contexts[context]['namespace']
    cluster = self.contexts[context]['cluster']
    deployments = api.list_namespaced_deployment(
      namespace=namespace,
      label_selector='varys-monitorable-target=true',
      watch=False,
    )
    for deployment in deployments.items:
      targets.append(protobuf_messages.Target(
        id=deployment.metadata.name,
        name=deployment.metadata.name,
        source=self.name,
        environment=protobuf_messages.CONTAINER,
        metadata={'cluster': cluster, 'namespace': namespace, 'context': context, 'kind': 'Deployment'},
      ))
    return targets

  def get_monitoring_system_status(self, monitoring_request_id, target, pattern, probes, monitoring_units, data_engine):
    # Only global status is currently supported
    return_status = protobuf_messages.UNKNOWN

    # Check specifically for each of the patterns
    if protobuf_messages.RESERVED_SIDECAR_SINGLE_PROBE == pattern:
      return_status = self._check_reserved_sidecar_single_probe_pattern_status(monitoring_request_id, target, probes, monitoring_units)

    # If the data engine needs reconfiguration, check if there are still post deployment artifacts
    # to determine if the return status has to be NEEDS_RECONFIGURATION
    if data_engine.needs_reconfiguration and return_status == protobuf_messages.DEPLOYED:
      post_deployment_artifacts_dir = abspath(join(self.POST_DEPLOYMENT_DIR, monitoring_request_id))
      if isdir(post_deployment_artifacts_dir):
        return protobuf_messages.NEEDS_RECONFIGURATION

    return return_status

  def _check_reserved_sidecar_single_probe_pattern_status(self, monitoring_request_id, target, probes, monitoring_units):
    context = target.metadata['context']
    namespace = target.metadata['namespace']
    apps_v1 = client.AppsV1Api(self.api_clients[context])
    return_status = protobuf_messages.UNKNOWN

    if target.metadata.get('kind') and target.metadata['kind'] == 'Deployment':
      response = apps_v1.read_namespaced_deployment_status(target.id, namespace)
    elif target.metadata.get('kind') and target.metadata['kind'] == 'StatefulSet':
      response = apps_v1.read_namespaced_stateful_set_status(target.id, namespace)
    else:
      raise RuntimeError('Kubernetes kind not supported or undefined')

    status = response.status

    # Let's check if the resource is on an error status...
    for condition in status.conditions:
      if (condition.type == 'Progressing' and not condition.status) or (condition.type == 'ReplicaFailure' and condition.status):
        return protobuf_messages.ERROR

    # It is not on an error status: let's check if it is already deployed...
    if target.metadata['kind'] == 'Deployment':
      if (status.updated_replicas == response.spec.replicas
        and status.replicas == response.spec.replicas
        and status.available_replicas == response.spec.replicas
        and status.observed_generation >= response.metadata.generation):
        return_status = protobuf_messages.DEPLOYED
    elif target.metadata['kind'] == 'StatefulSet':
      if (status.updated_replicas == response.spec.replicas
        and status.replicas == response.spec.replicas
        and status.ready_replicas == response.spec.replicas
        and status.observed_generation >= response.metadata.generation):
        return_status = protobuf_messages.DEPLOYED

    # Resource replicas are deployed, let's verify all the sidecars are in the Pod...
    if return_status == protobuf_messages.DEPLOYED:
      container_names = [ c.name for c in response.spec.template.spec.containers ]
      for monitoring_unit in monitoring_units:
        # If true, a sidecar disappeared from the Po{{ resource.metadata.labels['app'] }}d. Thus, we need to deploy again...
        if monitoring_unit.id not in container_names:
          return_status = protobuf_messages.NEEDS_REDEPLOYMENT
          break
      return return_status

    # Otherwise...The resource is regurarly progressing
    return protobuf_messages.DEPLOYING


  def reconfigure_prometheus(self, monitoring_request_id, hosts, credentials, config_location):
    context = credentials.get('context')
    namespace = credentials.get('namespace')
    core_v1 = client.CoreV1Api(self.api_clients[context])

    config_map = core_v1.read_namespaced_config_map(name=config_location, namespace=namespace)
    prometheus_yml = self.yaml.load(config_map.data['prometheus.yml'])

    probes_to_add, probes_to_remove = self._load_reconfiguration_prometheus_files(monitoring_request_id)

    # Delete jobs
    jobs_to_remove = []
    for configs in probes_to_remove:
      jobs_to_remove += [ c['job_name'] for c in configs ]
    new_scrape_configs = [ config for config in prometheus_yml['scrape_configs'] if config['job_name'] not in jobs_to_remove]

    # Add/Update jobs
    new_scrape_configs_job_names = [ config['job_name'] for config in new_scrape_configs ]
    for configs in probes_to_add:
      for c in configs:
        # If the job is not present, we have to add it
        if c['job_name'] not in new_scrape_configs_job_names:
          new_scrape_configs.append(c)
        # Otherwise, we have to find and replace the job with its updated version
        else:
          for idx, original_config in enumerate(new_scrape_configs):
            if c['job_name'] == original_config['job_name']:
              new_scrape_configs[idx] = c

    prometheus_yml['scrape_configs'] = new_scrape_configs
    config_map.data['prometheus.yml'] = self.yaml.dump(prometheus_yml)

    core_v1.replace_namespaced_config_map(name=config_location, namespace=namespace, body=config_map)

  def _load_reconfiguration_prometheus_files(self, monitoring_request_id):
    to_add_path = abspath(join(self.POST_DEPLOYMENT_DIR, monitoring_request_id, 'to_add'))
    to_remove_path = abspath(join(self.POST_DEPLOYMENT_DIR, monitoring_request_id, 'to_remove'))
    probes_to_add = []
    probes_to_remove = []
    for root, _, files in walk(to_add_path):
      for file in files:
        probes_to_add.append(self.yaml.load(Path(join(root, file))))
    for root, _, files in walk(to_remove_path):
      for file in files:
        probes_to_remove.append(self.yaml.load(Path(join(root, file))))

    return probes_to_add, probes_to_remove

  def apply_internal_probes_pattern(self, monitoring_request_id, target, probes_to_add, probes_to_remove, monitoring_units, data_engine):
    raise NotImplementedError(
      'The internal_probes pattern is not supported by the {} {} plugin'.format(self._alias_, self._version_)
    )

  def apply_reserved_sidecar_multi_probes_pattern(self, monitoring_request_id, target, probes_to_add, probes_to_remove, monitoring_units, data_engine):
    raise NotImplementedError(
      'The reserved_sidecar_multi_probes pattern is not supported by the {} {} plugin'.format(self._alias_, self._version_)
    )

  def apply_reserved_sidecar_single_probe_pattern(self, monitoring_request_id, target, probes_to_add, probes_to_remove, monitoring_units, data_engine):
    context = target.metadata['context']
    namespace = target.metadata['namespace']
    apps_v1 = client.AppsV1Api(self.api_clients[context])
    core_v1 = client.CoreV1Api(self.api_clients[context])

    if target.metadata.get('kind') and target.metadata['kind'] == 'Deployment':
      resource = apps_v1.read_namespaced_deployment(name=target.id, namespace=namespace)
    elif target.metadata.get('kind') and target.metadata['kind'] == 'StatefulSet':
      resource = apps_v1.read_namespaced_stateful_set(name=target.id, namespace=namespace)
    else:
      raise RuntimeError('Kubernetes resource type not supported or undefined')

    monitoring_units_to_add = []
    monitoring_units_to_remove = []

    sidecars_to_remove = []
    sidecars_to_add = []

    volumes_to_add = []
    volumes_to_remove = []

    # config_maps_to_add = []
    config_maps_to_remove = []

    # services_to_add = []
    services_to_remove = []

    for probe in probes_to_add:
      sidecar, volumes, config_maps, service = self._build_sidecar_elements(monitoring_request_id, resource, probe)
      monitoring_units_to_add.append(
        protobuf_messages.MonitoringUnit(
          id=sidecar.get('name'),
          name=sidecar.get('name'),
          probe_ids=[probe.id]
        )
      )
      sidecars_to_add.append(sidecar)
      volumes_to_add.extend(volumes)

      try:
        for config_map in config_maps:
          core_v1.create_namespaced_config_map(
            namespace=namespace,
            body=config_map
          )
      except ApiException as e:
        if 'Conflict' != e.reason:
          raise e
      try:
        service = core_v1.create_namespaced_service(
          namespace=namespace,
          body=service
        )
      except ApiException as e:
        if 'Conflict' != e.reason:
          raise e
      sidecar_service_details = self._build_sidecar_service_details(service, core_v1)
      self._create_post_deployment_configs(probe, 'to_add', monitoring_request_id, resource, data_engine, sidecar_service_details)

    for probe in probes_to_remove:
      sidecar, volumes, config_maps, service = self._build_sidecar_elements(monitoring_request_id, resource, probe)
      monitoring_units_to_remove.append(sidecar.get('name'))
      sidecars_to_remove.append(sidecar)
      volumes_to_remove.extend(volumes)
      config_maps_to_remove.extend(config_maps)
      services_to_remove.append(service)
      sidecar_service_details = self._build_sidecar_service_details(service, core_v1)
      self._create_post_deployment_configs(probe, 'to_remove', monitoring_request_id, resource, data_engine, sidecar_service_details)

    resource = self._delete_elements_from_resource(resource, sidecars_to_remove, volumes_to_remove)
    resource = self._add_elements_to_resource(resource, sidecars_to_add, volumes_to_add)
    resource_dict = self._prepare_resource_dict(resource)

    if target.metadata['kind'] == 'Deployment':
      apps_v1.replace_namespaced_deployment(
        name=target.id,
        namespace=namespace,
        body=resource_dict
      )
    elif target.metadata['kind'] == 'StatefulSet':
      apps_v1.replace_namespaced_stateful_set(
        name=target.id,
        namespace=namespace,
        body=resource_dict
      )

    try:
      # Collect config map names to be removed
      config_maps_to_remove = [cm.get('metadata').get('name') for cm in config_maps_to_remove]
      for config_map in config_maps_to_remove:
        core_v1.delete_namespaced_config_map(
          namespace=namespace,
          name=config_map
        )
    except ApiException as e:
      if 'NotFound' != e.reason:
        raise e

    try:
      # Collect service names to be removed
      services_to_remove = [s.get('metadata').get('name') for s in services_to_remove]
      for service in services_to_remove:
        core_v1.delete_namespaced_service(
          namespace=namespace,
          name=service
        )
    except ApiException as e:
      if 'NotFound' != e.reason:
        raise e

    current_monitoring_units = monitoring_units
    current_monitoring_units.extend(monitoring_units_to_add)
    current_monitoring_units = [mu for mu in current_monitoring_units if mu.id not in monitoring_units_to_remove]

    return current_monitoring_units

  def _build_sidecar_service_details(self, service, core_v1):
    if not isinstance(service, client.V1Service):
      name = service['metadata']['name']
      namespace = service['metadata']['namespace']
      service = core_v1.read_namespaced_service(name, namespace)

    sidecar_service_details = {
      'host': '',
      'port': ''
    }

    # Assuming the service exposes a single port...
    if 'NodePort' == service.spec.type:
      for p in service.spec.ports:
        if p.node_port:
          sidecar_service_details['port'] = p.node_port
          break
      node_addresses = core_v1.list_node().items[0].status.addresses
      for a in node_addresses:
        # This can lead to unreachable services if the network configs are more complex
        if 'InternalIP' == a.type or 'ExternalIP' == a.type:
          sidecar_service_details['host'] = a.address
          break
    elif 'ClusterIP' == service.spec.type:
      for p in service.spec.ports:
        if p.port:
          sidecar_service_details['port'] = p.port
          break
      sidecar_service_details['host'] = '{}.{}'.format(service.metadata.name, service.metadata.namespace)
    return sidecar_service_details

  def _build_sidecar_elements(self, monitoring_request_id, resource, probe):
    jinja_env = Environment(loader=FileSystemLoader(self.TEMPLATES_DIR), trim_blocks=True)
    config_maps = []
    volumes = []

    template_args = {
      'monitoring_request_id': monitoring_request_id,
      'cloud_property': probe.cloud_properties_to_monitor[0],
      'resource': resource,
      'md5': self._md5_jinja_filter
    }

    sidecar = self._load_sidecar(probe.name, jinja_env, template_args)
    volumes.extend(self._load_sidecar_volumes_for_pod(probe.name, jinja_env, template_args))
    config_maps.extend(self._load_sidecar_config_maps(probe.name, jinja_env, template_args))
    service = self._load_sidecar_service(probe.name, jinja_env, template_args)

    return sidecar, volumes, config_maps, service

  def _delete_elements_from_resource(self, resource, sidecars, volumes):
    for sidecar in sidecars:
      label_key = 'varys-{}-probe'.format(sidecar.get('name'))
      del resource.metadata.labels[label_key]
      del resource.spec.template.metadata.labels[label_key]

    # Extract only the names to make the remove process straightforward
    sidecars = [c.get('name') for c in sidecars]
    volumes = [v.get('name') for v in volumes]

    resource.spec.template.spec.containers = [c for c in resource.spec.template.spec.containers if c.name not in sidecars]
    if resource.spec.template.spec.volumes is not None:
      resource.spec.template.spec.volumes = [v for v in resource.spec.template.spec.volumes if v.name not in volumes]
    return resource

  def _add_elements_to_resource(self, resource, sidecars, volumes):
    resource.spec.template.spec.containers.extend(sidecars)

    if resource.spec.template.spec.volumes is None:
      resource.spec.template.spec.volumes = volumes
    else:
      resource.spec.template.spec.volumes.extend(volumes)

    for sidecar in sidecars:
      label_key = 'varys-{}-probe'.format(sidecar.get('name'))
      resource.metadata.labels[label_key] = 'true'
      resource.spec.template.metadata.labels[label_key] = 'true'

    return resource

  def _prepare_resource_dict(self, resource):
    resource_dict = resource.to_dict()

    # Clean up status and metadata read only fields
    resource_dict.pop('status', None)
    resource_dict['metadata'] = {
      'annotations': resource_dict.get('metadata', {}).get('annotations', {}),
      'labels': resource_dict.get('metadata', {}).get('labels', {}),
      'name': resource_dict.get('metadata', {}).get('name', ''),
      'namespace': resource_dict.get('metadata', {}).get('namespace', {}),
    }

    resource_dict = self._change_keys_notation(
      resource_dict,
      string_utils.snake_case_to_camel,
      {'upper_case_first': False},
      ['labels', 'annotations']
    )

    return resource_dict

  def _load_sidecar_service(self, probe_name, jinja_env, template_args):
    path = 'sidecars/{}/service.yml.j2'.format(probe_name)
    if not isfile(join(self.TEMPLATES_DIR, path)):
      return None

    template = jinja_env.get_template('sidecars/{}/service.yml.j2'.format(probe_name))
    return self.yaml.load(template.render(**template_args))

  def _load_sidecar_volumes_for_pod(self, probe_name, jinja_env, template_args):
    volumes = []
    path = 'sidecars/{}/volumes.yml.j2'.format(probe_name)

    if not isfile(join(self.TEMPLATES_DIR, path)):
      return volumes

    template = jinja_env.get_template(path)
    sidecar_volumes = self.yaml.load(template.render(**template_args))
    volumes.extend(sidecar_volumes)

    return volumes

  def _load_sidecar_config_maps(self, probe_name, jinja_env, template_args):
    config_maps = []
    config_maps_dir = 'sidecars/{}/config_maps'.format(probe_name)
    if not isdir(join(self.TEMPLATES_DIR, config_maps_dir)):
      return config_maps
    config_map_templates_names = [f for f in listdir(join(self.TEMPLATES_DIR, config_maps_dir))]

    for name in config_map_templates_names:
      template = jinja_env.get_template('{}/{}'.format(config_maps_dir, name))
      sidecar_config_map = self.yaml.load(template.render(**template_args))
      config_maps.append(sidecar_config_map)

    return config_maps

  def _load_sidecar(self, probe_name, jinja_env, template_args):
    template = jinja_env.get_template('sidecars/{}/container_spec.yml.j2'.format(probe_name))
    return self.yaml.load(template.render(**template_args))

  def _create_post_deployment_configs(self, probe, post_deployment_type, monitoring_request_id, resource, data_engine, sidecar_service_details):
    template_args = {
      'monitoring_request_id': monitoring_request_id,
      'resource': resource,
      'data_engine': data_engine,
      'sidecar_service_details': sidecar_service_details,
      'md5': self._md5_jinja_filter
    }
    jinja_env = Environment(loader=FileSystemLoader(self.TEMPLATES_DIR), trim_blocks=True)
    template = jinja_env.get_template('sidecars/{}/post_deployment.yml.j2'.format(probe.name))

    configs = self.yaml.load(template.render(**template_args))
    dest_path = Path(self.POST_DEPLOYMENT_DIR, monitoring_request_id, post_deployment_type, '{}.yml'.format(probe.name))
    self.yaml.dump(configs, dest_path)

  def _change_keys_notation(self, obj, transformation_function, tf_args, keys_blacklist):
    if isinstance(obj, (str, int, float)):
      return obj
    if isinstance(obj, dict):
      new = obj.__class__()
      for k, v in obj.items():
        if k in keys_blacklist:
          return obj
        new[transformation_function(k, **tf_args)] = self._change_keys_notation(v, transformation_function, tf_args, keys_blacklist)
    elif isinstance(obj, (list, set, tuple)):
      new = obj.__class__(self._change_keys_notation(v, transformation_function, tf_args, keys_blacklist) for v in obj)
    else:
      return obj
    return new

  def _md5_jinja_filter(self, string):
    return hashlib.md5(string.encode()).hexdigest()
