# -*- coding: utf-8 -*-

from environs import Env

env = Env()
env.read_env(recurse=True)

APP_NAME = env.str('APP_NAME', 'varys-monitoring-actuation-bridge')
DEBUG = env.bool('DEBUG', False)
RPC_SERVER_PORT = env.int('RPC_SERVER_PORT', 50051)

DISABLED_PLUGINS = env.list('DISABLED_PLUGINS')

KUBERNETES_CONFIG_FILE = env.str('KUBERNETES_CONFIG_FILE', '~/.kube/config')

OS_CLOUD = env.str('OS_CLOUD', 'openstack')
OS_CLIENT_CONFIG_FILE = env.str('OS_CLIENT_CONFIG_FILE', '~/.config/openstack/clouds.yaml')
OS_MONITORING_NETWORK_NAME = env.str('OS_MONITORING_NETWORK_NAME', 'monitoring-network')
OS_FLOATING_NETWORK_NAME = env.str('OS_FLOATING_NETWORK_NAME', 'floating-ip')
OS_MONITORING_PRIVATE_KEY = env.str('OS_MONITORING_PRIVATE_KEY', '~/.ssh/monitoring-key')
OS_RESERVED_SIDECAR_IMAGE_NAME = env.str('OS_RESERVED_SIDECAR_IMAGE_NAME', 'image-name')
OS_RESERVED_SIDECAR_FLAVOR_NAME = env.str('OS_RESERVED_SIDECAR_FLAVOR_NAME', 'flavor-name')
OS_RESERVED_SIDECAR_USERNAME = env.str('OS_RESERVED_SIDECAR_USERNAME', 'root')
OS_ANSIBLE_VAULT_PASSWORD = env.str('OS_ANSIBLE_VAULT_PASSWORD', 'secret')
