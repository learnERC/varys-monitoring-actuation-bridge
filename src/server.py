#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from concurrent import futures
from simple_settings import settings
import json
import logging
import time
import grpc
from google.protobuf import json_format
from app.api import monitoring_actuation_bridge_pb2 as protobuf_messages
from app.api import monitoring_actuation_bridge_pb2_grpc as protobuf_grpc
import pluginlib

_ONE_DAY_IN_SECONDS = 60 * 60 * 24

class MonitoringActuationBridgeServicer(protobuf_grpc.MonitoringActuationBridgeServicer):
  def __init__(self):
    super().__init__()
    self.plugins = {}

    _blacklist = []
    for plugin_name in settings.DISABLED_PLUGINS:
      _blacklist.append(('monitorable_platform', plugin_name))

    plugin_loader = pluginlib.PluginLoader(
      library='app.plugins',
      blacklist=_blacklist
    )
    for name, klass in plugin_loader.plugins.monitorable_platform.items():
      self.plugins[name] = klass()

  def ListTargets(self, request, context):
    """Lists the targets
    """
    try:
      targets = []
      for name, plugin in self.plugins.items():
        targets += plugin.list_targets(
          filters=(request.filters if request.filters is not None else {}),
        )
      return protobuf_messages.ListTargetsResponse(targets=targets)
    except Exception as e:
      context.set_code(grpc.StatusCode.ABORTED)
      context.set_details(str(e))
      return protobuf_messages.ListTargetsResponse()

  def GetMonitoringSystemStatus(self, request, context):
    """Gets the status of the monitoring system
    """
    try:
      plugin = self.plugins[request.target.source]
      status = plugin.get_monitoring_system_status(
        monitoring_request_id=request.monitoring_request_id,
        target=request.target,
        pattern=request.pattern,
        probes=request.probes,
        monitoring_units=(request.monitoring_units if request.monitoring_units is not None else []),
        data_engine=request.data_engine,
      )
      return protobuf_messages.GetMonitoringSystemStatusResponse(global_status=status)
    except Exception as e:
      context.set_code(grpc.StatusCode.ABORTED)
      context.set_details(str(e))
      return protobuf_messages.GetMonitoringSystemStatusResponse()

  def CreateMonitoringSystem(self, request, context):
    """Creates the monitoring system
    """
    try:
      plugin = self.plugins[request.target.source]
      monitoring_units = plugin.create_monitoring_system(
        monitoring_request_id=request.monitoring_request_id,
        target=request.target,
        pattern=request.pattern,
        probes=request.probes,
        data_engine=request.data_engine,
      )
      return protobuf_messages.CreateMonitoringSystemResponse(
        monitoring_units=(monitoring_units if monitoring_units is not None else []),
      )
    except Exception as e:
      context.set_code(grpc.StatusCode.ABORTED)
      context.set_details(str(e))
      return protobuf_messages.CreateMonitoringSystemResponse()

  def UpdateMonitoringSystem(self, request, context):
    """Updates the monitoring system
    """
    try:
      plugin = self.plugins[request.target.source]
      monitoring_units = plugin.update_monitoring_system(
        monitoring_request_id=request.monitoring_request_id,
        target=request.target,
        pattern=request.pattern,
        probes_to_add=request.probes_to_add,
        probes_to_remove=request.probes_to_remove,
        monitoring_units=(request.monitoring_units if request.monitoring_units is not None else []),
        data_engine=request.data_engine,
      )
      return protobuf_messages.UpdateMonitoringSystemResponse(
        monitoring_units=(monitoring_units if monitoring_units is not None else []),
      )
    except Exception as e:
      context.set_code(grpc.StatusCode.ABORTED)
      context.set_details(str(e))
      return protobuf_messages.UpdateMonitoringSystemResponse()

  def DeleteMonitoringSystem(self, request, context):
    """Deletes the monitoring system
    """
    try:
      plugin = self.plugins[request.target.source]
      plugin.delete_monitoring_system(
        monitoring_request_id=request.monitoring_request_id,
        target=request.target,
        pattern=request.pattern,
        probes=request.probes,
        monitoring_units=(request.monitoring_units if request.monitoring_units is not None else []),
        data_engine=request.data_engine,
      )
      return protobuf_messages.DeleteMonitoringSystemResponse()
    except Exception as e:
      context.set_code(grpc.StatusCode.ABORTED)
      context.set_details(str(e))
      return protobuf_messages.DeleteMonitoringSystemResponse()

  def ReconfigureDataEngine(self, request, context):
    """Reconfigures the data engine
    """
    try:
      plugin = self.plugins[request.data_engine.source]
      plugin.reconfigure_data_engine(
        monitoring_request_id=request.monitoring_request_id,
        data_engine=request.data_engine,
      )
      return protobuf_messages.ReconfigureDataEngineResponse()
    except Exception as e:
      context.set_code(grpc.StatusCode.ABORTED)
      context.set_details(str(e))
      return protobuf_messages.ReconfigureDataEngineResponse()

def serve():
  server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
  protobuf_grpc.add_MonitoringActuationBridgeServicer_to_server(MonitoringActuationBridgeServicer(), server)
  server.add_insecure_port('[::]:{}'.format(settings.RPC_SERVER_PORT))
  logging.debug('Starting server on port %d...', settings.RPC_SERVER_PORT)

  server.start()
  try:
    logging.debug('Server started!')
    while True:
      time.sleep(_ONE_DAY_IN_SECONDS)
  except KeyboardInterrupt:
    server.stop(0)

if __name__ == '__main__':
  if settings.DEBUG:
    level = logging.DEBUG
  else:
    level = logging.INFO

  logging.basicConfig(level=level, format='[%(asctime)s] %(levelname)s in %(module)s: %(message)s')
  serve()
